console.log("No. 1 (Animal Class)");
console.log("Release 0");

var Animal = class Animal {
    constructor (hewan) {
        this.name = hewan;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log();
console.log("Release 1");

class Ape extends Animal {
    constructor(hewan){
        super(hewan);
    }
    yell () {
        console.log("Auooo")
    }
}
class Frog extends Animal {
    constructor(hewan){
        super(hewan);
    }
    jump(){
        console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti");
sungokong.yell();
var kodok = new Frog("buduk");
kodok.jump();

console.log();
console.log();
console.log("Function to Class")

class Clock {
  constructor({template}){
    this.template = template;
  }
  render(){
    let date = new Date()
    
    let hours = date.getHours()
    if (hours < 10) hours = '0' + hours

    let mins = date.getMinutes()
    if (mins < 10) mins = '0' + mins

    let secs = date.getSeconds()
    if (secs < 10) secs = '0' + secs 

    let output = this.template
    .replace('h', hours)
    .replace('m', mins)
    .replace('s', secs)

    console.log(output)
  }
  stop(){
    clearInterval(this.timer)
  }
  start(){
    this.render();
    this.timer = setInterval(() => this.render, 1000);
  }
}

var clock = new Clock({template: 'h:m:s'});
  clock.start();