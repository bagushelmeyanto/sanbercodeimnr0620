console.log('No. 1 (Range)');

function range(firstNum, finishNum){
   var result = []
   if(firstNum < finishNum){
      for(var i = firstNum; i <= finishNum; i++){
         result.push(i)
      } return result
   } 
   else if(firstNum > finishNum){
      for(var j = firstNum; j >= finishNum; j--){
         result.push(j)
      } return result
   } 
   else {
      return -1;
   } 
}
console.log('1. ' + range(1, 10))
console.log('2. ' + range(1)) 
console.log('3. ' + range(11,18)) 
console.log('4. ' + range(54, 50))
console.log('5. ' + range())

console.log();
console.log();
console.log('No. 2 (Range With Step)')

function rangeWithStep(angka1, angka2, step){
   var hasil = []
   if(angka1 < angka2){
      for(var k = angka1; k <= angka2; k += step){
         hasil.push(k)
      }  
   } 
   else if(angka1 > angka2){
      for(var l = angka1; l >= angka2; l -= step){
         hasil.push(l)
      } 
   }
   else{; 
   }return hasil;
} 

console.log('1. ' + rangeWithStep(1, 10, 2)) 
console.log('2. ' + rangeWithStep(11, 23, 3)) 
console.log('3. ' + rangeWithStep(5, 2, 1)) 
console.log('4. ' + rangeWithStep(29, 2, 4)) 

console.log();
console.log();
console.log('No. 3 (Sum of Range)');

function sum(bin1, bin2, rentang){
   var total = 0
   if(rentang == undefined && bin1 < bin2){
      for(var e = bin1; e <= bin2; e++){
         total += e;
      } 
   } else if(rentang == undefined && bin1 > bin2){
      for(var f = bin1; f >= bin2; f--){
         total += f;
      } 
   } else if(bin1 < bin2){
      for(var g = bin1; g <= bin2; g += rentang){
         total += g;
      } 
   } else if(bin1 > bin2){
      for(var h = bin1; h >= bin2; h -= rentang){
         total += h;
      } 
   } else if(bin1 == undefined && bin2 == undefined && rentang == undefined){
      total == 0
   } else if(bin2 == undefined){
      var p = bin1 ;
      total =+ p
   } return total
   
}
console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum()) 

console.log();
console.log();
console.log('No. 4 (Array Multidimensi)');
var input = [
   ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
   ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
   ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
   ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(){
   for(var b = 0; b <= 3; b++){
      console.log('Nomor: '+ input[b][0]);
      console.log('Nama: '+ input[b][1]);
      console.log('TTL: '+ input[b][2] + ', ' + input[b][3]);
      console.log('Hobi: ' + input[b][4]);
      console.log();
   }
}
dataHandling()

console.log();
console.log();
console.log('No. 5 (Balik Kata)');


function balikKata(str){
   var normal = str
   var kebalik = '';
   
   for(var c = str.length - 1; c >= 0; c--){
       kebalik =  kebalik + normal[c]
       
   } return kebalik
}

console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 

console.log();
console.log();
console.log('No. 6 (Metode Array)');

var dataKu = [ "0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
dataKu.splice(4, 1, "Pria", "SMA Internasional Metro") 
dataKu.splice(1, 1, "Roman Alamsyah Elsharawy")
console.log(dataKu) 
console.log()

var tanggal = "21/05/1989"
var date = tanggal.split("/") // separasi ditandai dengan '/'
var bulan = Number(date[1])

switch(bulan){
   case 1: { console.log('Januari'); break;}
   case 2: { console.log('Februari'); break;}
   case 3: { console.log('Maret'); break;}
   case 4: { console.log('April'); break;}
   case 5: { console.log('Mei'); break;}
   case 6: { console.log('Juni'); break;}
   case 7: { console.log('Juli'); break;}
   case 8: { console.log('Agustus'); break;}
   case 9: { console.log('September'); break;}
   case 10: { console.log('Oktober'); break;}
   case 11: { console.log('November'); break;}
   case 12: { console.log('Desember')}

}

var sortir = date.sort(
   function(value1, value2){
      return value2 - value1
   }
) 
console.log(sortir)

var gabung = date.join('-')
console.log(gabung)

var nama = ["Roman", "Alamsyah", "Elsharawi"]
var batas = nama.slice(0,2)
var title = batas 
var slug = title.join(" ")
console.log(slug) 
