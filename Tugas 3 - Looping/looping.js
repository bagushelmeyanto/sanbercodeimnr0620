console.log('NO 1 - LOOPING WHILE');
console.log();

var jumlah = 0;
var jumlahSatu = 22;

console.log('LOOPING PERTAMA');

while(jumlah < 20){
    jumlah += 2;
    console.log(jumlah + ' - I love coding')
}

console.log('LOOPING KEDUA');

while(jumlahSatu > 0){
    jumlahSatu -= 2;
    console.log(jumlahSatu + ' - I will become a mobile developer')
}

console.log();
console.log();

console.log('NO. 2 LOOPING MENGGUNAKAN FOR')
console.log();

for(var angka = 1; angka < 21; angka++){
    if(angka%3 == 0 && angka%2 - 1 == 0) {
        console.log(angka + ' - I love coding')
    } else if(angka%2 == 0){
        console.log(angka + ' - Berkualitas')
    } else{
        console.log(angka + ' - Santai')
    }
}

console.log();
console.log();

console.log('NO. 3 MEMBUAT PERSEGI PANJANG');

for(var mulai = 1; mulai < 5; mulai++){
    console.log('########');
}

console.log();
console.log();

console.log('NO. 4 MEMBUAT TANGGA'); // ILL BE BACK

for(var beta = 1; beta < 8; beta++){
    if(beta == 1){
        console.log('#')
    } else if(beta == 2){
        console.log('##')
    } else if(beta == 3){
        console.log('###')
    } else if(beta == 4){
        console.log('####')
    } else if(beta == 5){
        console.log('#####')
    } else if(beta == 6){
        console.log('######')
    } else {
        console.log('#######')
    }
}

console.log();
console.log();

console.log('NO. 5 MEMBUAT PAPAN CATUR');

for(epsilon = 1; epsilon < 9; epsilon++){
    if(epsilon%2 == 0){
        console.log(' # # # #')
    } else{
        console.log('# # # # ')
    }
}