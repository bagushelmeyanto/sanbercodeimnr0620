// SOAL CONDITIONAL
console.log('Jawaban Conditional');
console.log();

var nama = 'Hasna';
var peran = '';

if( nama == ''){
    console.log('Nama harus diisi!')
} else if( peran == 'Werewolf'){
    console.log("Halo Werewolf " + nama + ', Kamu akan memakan mangsa setiap malam!' )
} else if( peran == 'Penyihir'){
    console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang jadi werewolf')
} else if( peran == 'Guard'){
    console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf')
} else {
    console.log('Halo ' + nama + ' pilih peranmu untuk memulai game!')
}


console.log();
console.log();

// SOAL SWITCH CASE
console.log('Jawaban Switch Case');
console.log();

var tanggal = 16 + '';
var bulan = 1;
var tahun = '' + 2003;

switch(bulan){
    case 1 : {bulan = 'Januari'; break;}
    case 2 : {bulan = 'Februari'; break;}
    case 3 : {bulan = 'Maret'; break;}
    case 4 : {bulan = 'April'; break;}
    case 5 : {bulan = 'Mei'; break;}
    case 6 : {bulan = 'Juni'; break;}
    case 7 : {bulan = 'Juli'; break;}
    case 8 : {bulan = 'Agustus'; break;}
    case 9 : {bulan = 'September';break;}
    case 10 : {bulan = 'Oktober'; break;}
    case 11 : {bulan = 'November'; break;}
    case 12 : {bulan = 'Desember'; break;}
}

console.log(tanggal + ' ' + bulan + ' ' +  tahun)